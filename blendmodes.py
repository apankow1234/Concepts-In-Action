include math

def PinLight( A, B ) :
	prt = 2 * A
	lss = prt - 1
	if B < lss :
		return prt - 1
	elif lss < B and B < prt :
		return B
	else :
		return prt



class Color :

	BLENDMODES = {
		#-------------------------#
		#      Normal  Modes      #
		#-------------------------#
		  "Normal"             : 0
		, "Dissolve"           : 1
		#-------------------------#
		#      Darken  Modes      #
		#-------------------------#
		, "Darken"             : 2
		, "Multiply"           : 3
		, "Color Burn"         : 4
		, "Linear Burn"        : 5
		, "Darker Color"       : 6
		#-------------------------#
		#      Lighten Modes      #
		#-------------------------#
		, "Lighten"            : 7
		, "Screen"             : 8
		, "Color Dodge"        : 9
		, "Linear Dodge"       : 10
		, "Add"                : 10
		, "Linear Dodge (Add)" : 10
		, "Linear Color"       : 11
		#-------------------------#
		#     Contrast  Modes     #
		#-------------------------#
		, "Overlay"            : 12
		, "Soft Light"         : 13
		, "Hard Light"         : 14
		, "Vivid Light"        : 15
		, "Linear Light"       : 16
		, "Pin Light"          : 17
		, "Hard Mix"           : 18
		#-------------------------#
		#     Inversion Modes     #
		#-------------------------#
		, "Difference"         : 19
		, "Exclusion"          : 20
		#-------------------------#
		#   Cancellation  Modes   #
		#-------------------------#
		, "Subtract"           : 21
		, "Divide"             : 22
		#-------------------------#
		#     Component Modes     #
		#-------------------------#
		, "Hue"                : 23
		, "Saturation"         : 24
		, "Color"              : 25
		, "Luminosity"         : 26
	}


	def __init__(self, R, G, B, A) :
		self.r = R
		self.g = G
		self.b = B
		self.a = A
		self.CompositeValue = ( R + G + B ) / 3

	def BlendWith(self, Other, Mode) :

		if isinstance(Mode, basestring):
			Mode = BLENDMODES[ Mode ]

		if Mode is 0 : # Normal
			return self

		if Mode is 1 : # Dissolve
			return Other if self.a != 0.0 else self

		if Mode is 2 : # Darken
			return Color(
					  min( self.r, Other.r )
					, min( self.g, Other.g )
					, min( self.b, Other.b )
					, min( self.a, Other.a )
				)

		if Mode is 3 : # Multiply
			return Color(
					  self.r * Other.r
					, self.g * Other.g
					, self.b * Other.b
					, self.a * Other.a
				)

		if Mode is 4 : # Color Burn
			return Color(
					  1 - ( 1 - Other.r ) / self.r
					, 1 - ( 1 - Other.g ) / self.g
					, 1 - ( 1 - Other.b ) / self.b
					, 1 - ( 1 - Other.a ) / self.a
				)

		if Mode is 5 : # Linear Burn
			return Color(
					  self.r + Other.r - 1
					, self.g + Other.g - 1
					, self.b + Other.b - 1
					, self.a + Other.a - 1
				)

		if Mode is 6 : # Darker Color
			return self if self.CompositeValue < Other.CompositeValue else Other

		if Mode is 7 : # Lighten
			return Color(
					  max( self.r, Other.r )
					, max( self.g, Other.g )
					, max( self.b, Other.b )
					, max( self.a, Other.a )
				)

		if Mode is 8 : # Screen
			return Color(
					  1 - ( 1 - self.r ) * ( 1 - Other.r )
					, 1 - ( 1 - self.g ) * ( 1 - Other.g )
					, 1 - ( 1 - self.b ) * ( 1 - Other.b )
					, 1 - ( 1 - self.a ) * ( 1 - Other.a )
				)

		if Mode is 9 : # Color Dodge
			return Color(
					  Other.r / ( 1 - self.r )
					, Other.g / ( 1 - self.g )
					, Other.b / ( 1 - self.b )
					, Other.a / ( 1 - self.a )
				)

		if Mode is 10 : # Linear Dodge (Add)
			return Color(
					  self.r + Other.r
					, self.g + Other.g
					, self.b + Other.b
					, self.a + Other.a
				)

		if Mode is 11 : # Lighter Color
			return self if self.CompositeValue > Other.CompositeValue else Other

		if Mode is 12 : # Overlay
			if self.CompositeValue < 0.5 :
				return Color(
					  2 * self.r * Other.r 
					, 2 * self.g * Other.g
					, 2 * self.b * Other.b
					, 2 * self.a * Other.a
				)
			else :
				return Color(
					  1 - 2 * ( 1 - self.r ) * ( 1 - Other.r )
					, 1 - 2 * ( 1 - self.g ) * ( 1 - Other.g )
					, 1 - 2 * ( 1 - self.b ) * ( 1 - Other.b )
					, 1 - 2 * ( 1 - self.a ) * ( 1 - Other.a )
				)

		if Mode is 13 : # Soft Light
			# W3C Draft
			if Other.CompositeValue < 0.5 :
				return Color(
					  self.r - ( 1 - (2 * Other.r)) * self.r * ( 1 - self.r)
					, self.g - ( 1 - (2 * Other.g)) * self.g * ( 1 - self.g)
					, self.b - ( 1 - (2 * Other.b)) * self.b * ( 1 - self.b)
					, self.a - ( 1 - (2 * Other.a)) * self.a * ( 1 - self.a)
				)
			else :
				if self.CompositeValue <= 0.25 :
					return Color(
						  (( (16 * self.r) - 12 ) * self.r + 4) * self.r
						, (( (16 * self.g) - 12 ) * self.g + 4) * self.g
						, (( (16 * self.b) - 12 ) * self.b + 4) * self.b
						, (( (16 * self.a) - 12 ) * self.a + 4) * self.a
					)
				else :
					return Color(
						  math.sqrt( self.r )
						, math.sqrt( self.g )
						, math.sqrt( self.b )
						, math.sqrt( self.a )
					)

		if Mode is 14 : # Hard Light
			if Other.CompositeValue < 0.5 :
				return Color(
					  2 * Other.r * self.r 
					, 2 * Other.g * self.g
					, 2 * Other.b * self.b
					, 2 * Other.a * self.a
				)
			else :
				return Color(
					  1 - 2 * ( 1 - Other.r ) * ( 1 - self.r )
					, 1 - 2 * ( 1 - Other.g ) * ( 1 - self.g )
					, 1 - 2 * ( 1 - Other.b ) * ( 1 - self.b )
					, 1 - 2 * ( 1 - Other.a ) * ( 1 - self.a )
				)

		if Mode is 15 : # Vivid Light
			if self.CompositeValue

		if Mode is 16 : # Linear Light
			return Color(
				  Other.r + 2 * self.r - 1
				, Other.g + 2 * self.g - 1
				, Other.b + 2 * self.b - 1
				, Other.a + 2 * self.a - 1
			)

		if Mode is 17 : # Pin Light
			return Color(
				  PinLight( self.r, Other.r )
				, PinLight( self.g, Other.g )
				, PinLight( self.b, Other.b )
				, PinLight( self.a, Other.a )
			)

		if Mode is 18 : # Hard Mix
			pass

		if Mode is 19 : # Difference
			return Color(
				  abs( self.r - Other.r )
				, abs( self.g - Other.g )
				, abs( self.b - Other.b )
				, abs( self.a - Other.a )
			)

		if Mode is 20 : # Exclusion
			return Color(
				  0.5 - ( 2 * ( Other.r - 0.5 ) * ( self.r - 0.5 ) )
				, 0.5 - ( 2 * ( Other.g - 0.5 ) * ( self.g - 0.5 ) )
				, 0.5 - ( 2 * ( Other.b - 0.5 ) * ( self.b - 0.5 ) )
				, 0.5 - ( 2 * ( Other.a - 0.5 ) * ( self.a - 0.5 ) )
			)

		if Mode is 21 : # Subtract
			return Color(
				  Other.r - self.r
				, Other.g - self.g
				, Other.b - self.b
				, Other.a - self.a
			)

		if Mode is 22 : # Divide
			return Color(
				  Other.r / self.r
				, Other.g / self.g
				, Other.b / self.b
				, Other.a / self.a
			)

		if Mode is 23 : # Hue
			pass

		if Mode is 24 : # Saturation
			pass

		if Mode is 25 : # Color
			pass

		if Mode is 18 : # Luminosity
			pass
